(function () {
    var app = angular.module("app.login");

    app.component("login", {
        templateUrl: "app/login/login.template.html",
        controller: function (loginService, $state, localStorageService, $localStorage) {
            var vm = this;
            vm.$storage = $localStorage.$default();

            vm.logIn = function () {
                // vm.$storage.admin = false;
                // vm.$storage.userName = "Diana";
                // vm.$storage.userSurname = "Kanajeva";
                // vm.$storage.view = true;
                // $state.go('view');
                loginService.login(vm.data).then(function(response) {
                    vm.$storage.admin = response.admin;
                    vm.$storage.userName = response.name;
                    vm.$storage.userSurname = response.surname;
                    vm.$storage.view = true;
                    $state.go('view');
                }, function(reason) {
                    console.log(reason);
                });
            }
        }
    })

})();
