package pentacore.mvc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import pentacore.mvc.model.Report;
import pentacore.mvc.model.User;
import pentacore.mvc.repository.UserRepository;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;


@Service("ReportService")
public class ReportService {

    @Autowired
    private UserRepository userRepository;


    @Async
    public Future<User> LongOperation(){

            User user = userRepository.findById(1L);

            try {
                System.out.println(Thread.currentThread().getName());
                Thread.sleep(10000);
                System.out.println("End of async");
                return new AsyncResult<>(user);
            } catch (InterruptedException e) {
                throw new RuntimeException();
            }

    }

}
