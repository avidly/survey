(function () {
    var app = angular.module("app.surveyAnswer");

    app.component("surveyAnswer", {
        templateUrl: "app/surveyAnswer/surveyAnswer.template.html",
        controller: function (surveyAnswerService, $state) {
            var vm = this;
            vm.types = [{
                val: 'one',
                name: 'Vienas galimas pasirinkimas'
            },
            {
                val: 'many',
                name: 'Keli galimi pasirinkimai'
            },
            {
                val: 'area',
                name: 'Laisvas tekstas'
            },
            {
                val: 'integer',
                name: 'Sveikas skaičius'
            }];
            vm.survey = {};
            vm.change= [];

            vm.$onInit = function () {
                surveyAnswerService.load($state.params.id).then(function(response) {
                    vm.survey = response;
                     for (var i = 0; i < vm.survey.questionList.length; i++){
                        vm.change[i] = false;
                        if (vm.survey.questionList[i].type == "many" || vm.survey.questionList[i].type == "one"){
                            for (var j = 0; j < vm.survey.questionList[i].answerList.length; j++){
                                vm.survey.questionList[i].answerList[j].userAnswerList = [];
                                vm.survey.questionList[i].answerList[j].userAnswerList.push({"response": false});
                            }
                        } else if (vm.survey.questionList[i].type == "area") {
                            vm.survey.questionList[i].answerList[0].userAnswerList = [];
                            vm.survey.questionList[i].answerList[0].userAnswerList.push({"response": null});
                        } else if (vm.survey.questionList[i].type == "integer") {
                            vm.survey.questionList[i].answerList[0].userAnswerList = [];
                            vm.survey.questionList[i].answerList[0].userAnswerList.push({"response": null});
                         }
                    }
                }, function(reason) {
                    console.log(reason);
                });
            };

            vm.isRadioRequired = function(radioList) {
                for (var i = 0; i < radioList.length; i++) {
                    if (radioList[i].userAnswerList[0].response == true){
                        return true;
                    }
                }
                return false;
            };

            vm.done = function(formValidation) {
                var checked = false;
                if(formValidation) {
                    for (var i = 0; i < vm.survey.questionList.length; i++) {
                        checked = false;
                        if (vm.survey.questionList[i].type == "many"){
                            if(vm.survey.questionList[i].required == true) {
                                for (var j = 0; j < vm.survey.questionList[i].answerList.length; j++) {
                                    if (vm.survey.questionList[i].answerList[j].hasOwnProperty('userAnswerList')) {
                                        if (vm.survey.questionList[i].answerList[j].userAnswerList[0].response == true) {
                                            checked = true;
                                        }
                                    }
                                }
                                if (checked == false) {
                                    console.log(vm.survey.questionList[i].type);
                                    return;
                                }
                            }
                        } else if (vm.survey.questionList[i].type == "one") {
                            if (vm.survey.questionList[i].userAnswerList[0].response == null && vm.survey.questionList[i].required == true){
                                console.log(vm.survey.questionList[i].type);
                                console.log("hi");
                                return;
                            }
                        }

                        if (vm.survey.questionList[i].type == "integer"){
                            if(vm.survey.questionList[i].answerList[0].userAnswerList[0].response == null && vm.survey.questionList[i].required == true){
                                return;
                            } else if(vm.survey.questionList[i].answerList[0].userAnswerList[0].response != null){
                                if(vm.survey.questionList[i].answerList[0].answerString > vm.survey.questionList[i].answerList[0].userAnswerList[0].response || vm.survey.questionList[i].answerList[1].answerString < vm.survey.questionList[i].answerList[0].userAnswerList[0].response){
                                    console.log("maziau");
                                    console.log(vm.survey.questionList[i].answerList[0].userAnswerList[0].response);
                                    return;
                                }
                            }

                        }
                    }
                    for (var i = 0; i < vm.survey.questionList.length; i++) {
                        if (vm.survey.questionList[i].type == "many"){
                            for (var j = 0; j < vm.survey.questionList[i].answerList.length; j++) {
                                if (vm.survey.questionList[i].answerList[j].hasOwnProperty('userAnswerList')) {
                                    if (vm.survey.questionList[i].answerList[j].userAnswerList[0].response == false) {
                                        delete vm.survey.questionList[i].answerList[j].userAnswerList;
                                        j--;
                                    }
                                }

                            }
                        } else if (vm.survey.questionList[i].type == "one") {
                            if (vm.survey.questionList[i].userAnswerList[0].response != null){
                                vm.survey.questionList[i].answerList[vm.survey.questionList[i].userAnswerList[0].response].userAnswerList = [];
                                vm.survey.questionList[i].answerList[vm.survey.questionList[i].userAnswerList[0].response].userAnswerList.push({"response": true});
                            }
                            delete vm.survey.questionList[i].userAnswerList;

                        }
                    }
                    surveyAnswerService.done(vm.survey).then(function (response) {
                        $state.go('viewSurveys');
                    }, function (reason) {
                        console.log(reason);
                    });
                }

            };
        }
    });
})();
 