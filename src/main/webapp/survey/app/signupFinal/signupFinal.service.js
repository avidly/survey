(function () {
    var module = angular.module("app.signupFinal");

    module.service("signupFinalService", function ($http, $q) {

        this.signup = function (token) {
            var deferred = $q.defer();
            delete token.repeat;
            // Siųsti duomenis į serverį užregistravimui
            $http({
                method: "POST",
                url: "http://localhost:8080/web-app/updateuser/",
                data: token
            }).then(function (response) {
                if (response.status == 200) {
                    deferred.resolve(response);
                }
            }, function (response) {
                console.log('rejected', response);
            });

            return deferred.promise;
        };     

    })

})();
