(function () {

    angular.module("app", [
        /* external dependencies */
        "ui.router",

        /* app dependencies */
        "app.header",
        "app.login",
        "app.signup",
        "app.signupFinal",
        "app.reminder",
        "app.sidenav",
        "app.main",
        "app.content",
        "app.manage",
        "app.survey",
        "app.surveyAnswer",
        "app.report",
        "app.viewSurveys"

    ]);

})();