package pentacore.mvc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pentacore.mvc.model.Answer;


public interface AnswerRepository extends JpaRepository<Answer, Long> {
}
