(function () {
    var app = angular.module("app.signup");

    app.component("signup", {
        templateUrl: "app/signup/signup.template.html",
        controller: function (signupService, $state) {
            var vm = this;

            vm.done = false;

            vm.signup = function () {
                vm.done = true;
                signupService.signup(vm.email).then(
                    function(data) {
                        vm.done = true;
                    },
                    function(reason) {
                        console.log(reason);
                    }
                );
            };
        }
    });

})();
