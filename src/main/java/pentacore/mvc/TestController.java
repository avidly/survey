package pentacore.mvc;

import org.hibernate.Hibernate;
import org.hibernate.OptimisticLockException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import pentacore.mvc.model.*;
import pentacore.mvc.repository.AnswerRepository;
import pentacore.mvc.repository.QuestionRepository;
import pentacore.mvc.repository.SurveyRepository;
import pentacore.mvc.repository.UserRepository;
import pentacore.mvc.services.ApplicationMailer;
import pentacore.mvc.services.ReportService;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
public class TestController {

    @Autowired
    private SurveyRepository surveyRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    @Qualifier("mailServiceCool")
    private ApplicationMailer mailer;


    @Autowired
    private ReportService reportService;

    Future<User> asyncUser = null;

    @RequestMapping("/test")
    public void test(){

        //Survey survey = new Survey();
       //survey.setSurveyName("bum");

        //surveyRepository.save(survey);
        Survey survey = surveyRepository.findById(28);
        System.out.println(survey.getUserId());


        //Send a composed mail
        //mailer.sendMail("arasbraziunas10@gmail.com", "Test Subject", "Testing body");

        System.out.println("Suveike testas");
    }

    @RequestMapping(value = "/surveys/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Survey>> listAllSurveys() {
        List<Survey> surveys = surveyRepository.findAll();
        if(surveys.isEmpty()){
            return new ResponseEntity<List<Survey>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Survey>>(surveys, HttpStatus.OK);
    }

    @RequestMapping(value = "/allusers/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> listAllUsers() {
        List<User> users = userRepository.findAll();
        if(users.isEmpty()){
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }

    @RequestMapping(value = "/surveys/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Survey> getSurvey(@PathVariable("id") long id) {
        System.out.println("Fetching User with id " + id);
        Survey survey = surveyRepository.findById(id);
        if (survey == null) {
            System.out.println("User with id " + id + " not found");
            return new ResponseEntity<Survey>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Survey>(survey, HttpStatus.OK);
    }


    @RequestMapping(value = "/newsurvey/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Survey> postSurvey(@RequestBody Survey survey) {

        for (Question q : survey.getQuestionList()){
            q.setSurveyId(survey);
            for (Answer a : q.getAnswerList()){
                a.setQuestionId(q);
            }
        }
        surveyRepository.save(survey);

        return new ResponseEntity<Survey>(survey, HttpStatus.OK);
    }

    @RequestMapping(value = "/newuser/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> postUser(@RequestBody User user) {


        userRepository.save(user);
        //mailer.sendMail(user.getEmail(), "Password link", user.getId().toString());

        return new ResponseEntity<User>(user,HttpStatus.OK);
    }


    //http://localhost:8080/web-app/survey/#!/signup/5
    @RequestMapping(value = "/updateuser/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> updateUser(@RequestBody User user) {

        User currentUser = userRepository.findById(user.getId());
        //User currentUser = userRepository.findById(user.getId());
        currentUser.setName(user.getName());
        currentUser.setSurname(user.getSurname());
        currentUser.setPassword(user.getPassword());

        userRepository.save(currentUser);

        return new ResponseEntity<User>(currentUser,HttpStatus.OK);
    }


    @RequestMapping(value = "/login/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@RequestBody User user) {

        System.out.println(user.getEmail());
        User currentuser = userRepository.findByEmail(user.getEmail());
        System.out.println(currentuser.getEmail());
        currentuser.setSurveyList(surveyRepository.findAllByUserId(currentuser));

        System.out.println(currentuser.getPassword().equals(user.getPassword()));

        if((currentuser == null) || !(currentuser.getPassword().equals(user.getPassword()))){
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<User>(currentuser,HttpStatus.OK);
        }
    }


    @RequestMapping(value = "/long/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> longOp() throws ExecutionException, InterruptedException {

        System.out.println(Thread.currentThread().getName());
        if(asyncUser == null) {
            asyncUser = reportService.LongOperation();
        } else {
            return new ResponseEntity<User>(asyncUser.get(), HttpStatus.OK);
        }
        System.out.println("I am back");
        System.out.println(Thread.currentThread().getName());
        return new ResponseEntity<User>(HttpStatus.OK);
    }

    @RequestMapping(value = "/updateadmin/", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> updateAdmin(@RequestBody User user) {
        try{
            userRepository.save(user);
        }catch (ObjectOptimisticLockingFailureException exception){
            System.out.println("Pagavau");
            return new ResponseEntity<User>(HttpStatus.CONFLICT);
        }


        return new ResponseEntity<User>(user,HttpStatus.OK);
    }


    @RequestMapping(value = "/new/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Question> postQuestion(@RequestBody Question question) {


            for (Answer a : question.getAnswerList()){
                a.setQuestionId(question);
            }

        questionRepository.save(question);

        return new ResponseEntity<Question>(question, HttpStatus.OK);
    }


}
