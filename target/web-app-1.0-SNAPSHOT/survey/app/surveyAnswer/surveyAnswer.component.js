(function () {
    var app = angular.module("app.surveyAnswer");

    app.component("surveyAnswer", {
        templateUrl: "app/surveyAnswer/surveyAnswer.template.html",
        controller: function (loginService, $state) {
            var vm = this;

            vm.types = [{
                val: 'one',
                name: 'Vienas galimas pasirinkimas'
            },
            {
                val: 'many',
                name: 'Keli galimi pasirinkimai'
            },
            {
                val: 'area',
                name: 'Laisvas tekstas'
            },
            {
                val: 'integer',
                name: 'Sveikas skaičius'
            }];

            vm.survey = {
                name: null,
                about: null,
                public: false,
                questions: [{
                    questionName: null,
                    questionType: 'one',
                    mandatory: null,
                    answers : [
                        {
                            text: null
                        },
                        {
                            text: null
                        }
                    ]
                }]
            }

            vm.addNewQuestion = function (index){
                var obj = {
                    questionName: null,
                    questionType: 'one',
                    mandatory: null,
                    answers : [
                        {
                            text: null
                        },
                        {
                            text: null
                        }
                    ]
                };

                vm.survey.questions.splice(index+1, 0, obj);

                console.log(JSON.stringify(vm.survey));
            };

            vm.removeQuestion = function (index){
                vm.survey.questions.splice(index, 1);
            };

            vm.addNewAnswer = function (index){
                vm.survey.questions[index].answers.push({
                    text: null
                });
            };

            vm.removeAnswer = function (qIndex, ansIndex){
                vm.survey.questions[qIndex].answers.splice(ansIndex, 1);
            };
        }
    });
})();
 