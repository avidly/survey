(function () {
    var app = angular.module("app");


    app.config(function ($stateProvider, $urlRouterProvider) {
        var loginState = {
            name: 'login',
            url: '/login',
            component: 'login'
        };

        var signupState = {
            name: 'signup',
            url: '/signup',
            component: 'signup'
        };

        var signupFinalState = {
            name: 'signupFinal',
            url: '/signup/:id',
            component: 'signupFinal',
            params: {
                id: null
            }
        };

        var contentState = {
            abstract: true,
            name: 'content',
            url: '/app',
            component: 'content'
        };

        var manageState = {
            name: 'manage',
            parent: 'content',
            url: '/manage',
            component: 'manage'
        };

        var surveyState = {
            name: 'survey',
            parent: 'content',
            url: '/survey',
            component: 'survey'
        };

        var surveyAnswerState = {
            name: 'surveyAnswer',
            parent: 'content',
            url: '/surveyAnswer',
            component: 'surveyAnswer'
        };

        var viewState = {
            name: 'view',
            parent: 'content',
            url: '/view',
            component: 'view'

        };

        var raportState = {
            name: 'raport',
            parent: 'content',
            url: '/raport',
            component: 'raport'
        };
          
        $stateProvider.state(loginState);
        $stateProvider.state(signupState);
        $stateProvider.state(signupFinalState);
        $stateProvider.state(contentState);

        $stateProvider.state(manageState);
        $stateProvider.state(surveyState);
        $stateProvider.state(surveyAnswerState);
        $stateProvider.state(viewState);
        $stateProvider.state(raportState);
        
        $urlRouterProvider.otherwise('/login');
    });
})();