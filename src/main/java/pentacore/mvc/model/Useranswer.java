package pentacore.mvc.model;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "useranswer")
public class Useranswer implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer userAnswerId;

    @Column(name = "response")
    private String response;

    @JoinColumn(name = "answer_id", referencedColumnName = "id")
    @ManyToOne
    private Answer answerId;

}
