package pentacore.mvc.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "answer")
public class Answer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @JsonProperty
    public Long id;

    @Column(name = "answerstring")
    private String answerString;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "question_id")
    public Question questionId;

    @JsonIgnore
    @OneToMany(mappedBy = "answerId")
    private List<Useranswer> userAnswerList = new ArrayList<Useranswer>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAnswerString() {
        return answerString;
    }

    public void setAnswerString(String answerString) {
        this.answerString = answerString;
    }

    public Question getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Question questionId) {
        this.questionId = questionId;
    }

    public List<Useranswer> getUserAnswerList() {
        return userAnswerList;
    }

    public void setUserAnswerList(List<Useranswer> userAnswerList) {
        this.userAnswerList = userAnswerList;
    }
}
