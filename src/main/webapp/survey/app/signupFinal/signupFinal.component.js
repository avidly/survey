(function () {
    var app = angular.module("app.signupFinal");

    app.component("signupFinal", {
        templateUrl: "app/signupFinal/signupFinal.template.html",
        controller: function (signupFinalService, $state) {
            var vm = this;

            vm.signup = function () {
                vm.data.id = $state.params.id;
                signupFinalService.signup(vm.data).then(
                    function() {
                        $state.go('login');
                    }, 
                    function(reason) {
                        console.log(reason);
                    }
                );

            };
        }
    });

})();
