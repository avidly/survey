(function () {
var app = angular.module("app.manage");

    app.component("manage", {
        templateUrl: "app/manage/manage.template.html",
        controller: function ($state, $localStorage, manageService) {
            vm = this;
            vm.$storage = $localStorage.$default();
            vm.userList = [];
            vm.showMessage = false;
            // vm.userList = [{
            //     email: 'vardenis.pavardenis@mail.lt',
            //     admin: false,
            //     blocked: false
            // },
            // {
            //     email: 'vardenis.pavardenis2@mail.lt',
            //     admin: false,
            //     blocked: true
            // },
            // {
            //     email: 'vardenis.pavardenis3@mail.lt',
            //     admin: true,
            //     blocked: false
            // },
            // {
            //     email: 'vardenis.pavardenis4@mail.lt',
            //     admin: true,
            //     blocked: true
            // }];

            vm.$onInit = function () {
                if (!vm.$storage.view) {
                    $state.go('login');
                }
                vm.load();

            };

            vm.load =function(){
                manageService.loadUsers().then(function(response) {
                    vm.userList = response;
                }, function(reason) {
                    console.log(reason);
                });
            };

            vm.update = function (index) {
                manageService.updateUsers(vm.userList[index]).then(function() {
                    vm.load();
                }, function(reason) {
                    if (reason.status == 409) {
                        vm.showMessage = true;
                    }
                });
            };
        }
    });
})();
 