(function () {

    angular.module("app", [
        /* external dependencies */
        "ui.router",

        /* app dependencies */
        "app.header",
        "app.login",
        "app.signup",
        "app.signupFinal",
        "app.sidenav",
        "app.main",
        "app.content",
        "app.manage",
        "app.survey",
        "app.viewSurveys",
        "app.raportSurveys",
        "app.list"

    ]);

})();