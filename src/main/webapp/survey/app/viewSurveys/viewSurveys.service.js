(function () {
    var module = angular.module("app.viewSurveys");

    module.service("viewSurveysService", function ($http, $q) {

        this.loadSurevys = function (token) {
            var deferred = $q.defer();
            $http({
                method: "GET",
                url: "http://localhost:8080/web-app/surveys/"
            }).then(function (response) {
                if (response.status == 200) {
                    deferred.resolve(response.data);
                }
            }, function (response) {
                deferred.reject(response);
            });

            return deferred.promise;
        };

        this.deleteSurvey = function (token) {
            var deferred = $q.defer();
            $http({
                method: "POST",
                url: "http://localhost:8080/web-app/delete/" + token
            }).then(function (response) {
                if (response.status == 200) {
                    deferred.resolve(response);
                }
            }, function (response) {
                deferred.reject(response);
            });

            return deferred.promise;
        };

        this.uploadFiles = function (token) {
            var deferred = $q.defer();

            var file = new FormData();
            file.append('file', token);

            $http({
                method: "POST",
                url: "http://localhost:8080/web-app/uploadExcelFile/",
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined},
                data: file
            }).then(function (response) {
                if (response.status == 200) {
                    deferred.resolve(response);
                }
            }, function (response) {
                deferred.reject(response);
            });

            return deferred.promise;
        };
    })

})();
