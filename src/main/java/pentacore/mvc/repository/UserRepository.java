package pentacore.mvc.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import pentacore.mvc.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findById(Long id );
    User findByEmail(String email);
}
