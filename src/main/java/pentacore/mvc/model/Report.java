package pentacore.mvc.model;

import java.io.Serializable;


public class Report implements Serializable {

    private String finished;

    public void setFinished(String finished) {
        this.finished = finished;
    }

    public String getFinished() {
        return finished;
    }
}
