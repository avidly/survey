(function () {
    var module = angular.module("app.survey");

    module.service("surveyService", function ($http, $q) {

        this.save = function (token) {
            var deferred = $q.defer();
            $http({
                method: "POST",
                url: "http://localhost:8080/web-app/newsurvey/",
                data: token
            }).then(function (response) {
                if (response.status == 200) {
                    deferred.resolve();
                }
            }, function (response) {
                console.log('rejected', response);
            });

            return deferred.promise;
        };

    })

})();
