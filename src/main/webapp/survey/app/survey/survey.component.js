(function () {
    var app = angular.module("app.survey");

    app.component("survey", {
        templateUrl: "app/survey/survey.template.html",
        controller: function ($localStorage, surveyService, $state) {
            var vm = this;
            vm.$storage = $localStorage.$default();
            vm.types = [{
                val: 'one',
                name: 'Vienas galimas pasirinkimas'
            },
            {
                val: 'many',
                name: 'Keli galimi pasirinkimai'
            },
            {
                val: 'area',
                name: 'Laisvas tekstas'
            },
            {
                val: 'integer',
                name: 'Sveikas skaičius'
            }];
            vm.survey = {
                surveyName: null,
                surveyDescription: null,
                open: false,
                userEmail: vm.$storage.email,
                questionList: [{
                    question: null,
                    type: 'one',
                    required: null,
                    answerList : [
                        {
                            answerString: null
                        },
                        {
                            answerString: null
                        }
                    ]
                }]
            };

            vm.$onInit = function () {
                if (!vm.$storage.view) {
                    $state.go('login');
                }
            };

            vm.addNewQuestion = function (index){
                var obj = {
                    question: null,
                    type: 'one',
                    required: null,
                    answerList : [
                        {
                            answerString: null
                        },
                        {
                            answerString: null
                        }
                    ]
                };

                vm.survey.questionList.splice(index+1, 0, obj);
            };

            vm.removeQuestion = function (index){
                vm.survey.questionList.splice(index, 1);
            };

            vm.addNewAnswer = function (index){
                vm.survey.questionList[index].answerList.push({
                    answerString: null
                });
            };

            vm.removeAnswer = function (qIndex, ansIndex){
                vm.survey.questionList[qIndex].answerList.splice(ansIndex, 1);
            };

            vm.saveSurvey = function (formValidation) {
                for (var i = 0; i < vm.survey.questionList.length; i++){
                    if(vm.survey.questionList[i].type == "integer" && vm.survey.questionList[i].answerList[0].answerString >= vm.survey.questionList[i].answerList[1].answerString) {
                        return;
                    }
                }
                if (formValidation) {
                    surveyService.save(vm.survey).then(function () {
                        $state.go('viewSurveys');
                    }, function (reason) {
                        console.log(reason);
                    });
                }
            }

        }
    });
})();
 