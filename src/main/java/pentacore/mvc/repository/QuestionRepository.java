package pentacore.mvc.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import pentacore.mvc.model.Question;
import pentacore.mvc.model.Survey;

import java.util.List;


public interface QuestionRepository extends JpaRepository<Question, Long> {
    List<Question> findAllBySurveyId(Survey surveyId);
}
