package pentacore.mvc.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "survey")
public class Survey implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @JsonProperty
    private Long id;

    @Column(name = "name")
    private String surveyName;

    @Column(name = "description")
    private String surveyDescription;


    @Column(name = "open")
    private Boolean open;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    public User userId;

    @JsonProperty
    @OneToMany(mappedBy = "surveyId", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public List<Question> questionList = new ArrayList<Question>();

    @JsonProperty
    @CreationTimestamp
    @Column(name = "create_date")
    private Date createDate;

    // Perdaryt sita butinai!!
    @Override
    public String toString(){
        return this.questionList.get(0).id.toString()+" joooo";
    }

    public void setSurveyName(String surveyName) {
        this.surveyName = surveyName;
    }

    public void setSurveyDescription(String surveyDescription) {
        this.surveyDescription = surveyDescription;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSurveyName() {
        return surveyName;
    }

    public String getSurveyDescription() {
        return surveyDescription;
    }

    public List<Question> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(List<Question> questionList) {
        this.questionList = questionList;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Boolean getOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

}
