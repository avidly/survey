(function () {
    var module = angular.module("app.manage");

    module.service("manageService", function ($http, $q) {

        this.loadUsers = function (token) {
            var deferred = $q.defer();
            $http({
                method: "GET",
                url: "http://localhost:8080/web-app/allusers/"
            }).then(function (response) {
                if (response.status == 200) {
                    deferred.resolve(response.data);
                }
            }, function (response) {
                console.log('rejected', response);
            });

            return deferred.promise;
        };

        this.updateUsers = function (token) {
            var deferred = $q.defer();
            $http({
                method: "PUT",
                url: "http://localhost:8080/web-app/updateadmin/",
                data: token
            }).then(function (response) {
                if (response.status == 200) {
                    deferred.resolve(response.data);
                }
            }, function (response) {
                console.log('rejected', response);
                deferred.reject(response);
            });

            return deferred.promise;
        };

    })

})();
